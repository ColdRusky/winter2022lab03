import java.util.Scanner;

public class Shop{
	
	public static void main(String[] args){
		
		//Creating an input for the user
		Scanner reader = new Scanner(System.in);
		
		//Creating an array of 4 objects/movies
		Movie[] myMovieList = new Movie[4];
		
		//Populating the array with a for loop
		for (int i = 0; i < myMovieList.length; i++){
			
			myMovieList[i] = new Movie();
			
			//Asking the user to input the title of the movie
			System.out.println("Enter the title of the movie: ");
			myMovieList[i].title = reader.next();
			
			//Asking the user to input the release date of the movie
			System.out.println("Enter the release date of the movie: ");
			myMovieList[i].releaseDate = reader.nextInt();
			
			//Asking the user to input the director of the movie
			System.out.println("Enter the director of the movie: ");
			myMovieList[i].director = reader.next();
		}
		
		//Displaying the title of the 4th/last movie
		System.out.println("The title of the last movie: "+ myMovieList[3].title);
		
		//Displaying the release date of the 4th/last movie
		System.out.println("The release date of the last movie: "+ myMovieList[3].releaseDate);
		
		//Displaying the director of the 4th/last movie
		System.out.println("The director of the last movie: "+ myMovieList[3].director);
		
		//Calling the printTitle instance method from the Movie class to display the title of the 4th/last movie 
		myMovieList[3].printTitle();
	}


}