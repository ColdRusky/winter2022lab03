public class Movie{
	
	//Creating 3 fields
	public String title;
	public int releaseDate;
	public String director;
	
	//An instance method that uses 1 of the fields and returns void             
	public void printTitle(){
		
		//Displaying the title field of the 4th/last movie to the user
		System.out.println("This is the title of the last movie: "+ this.title);

}
}